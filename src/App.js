import { Container } from 'react-bootstrap';
import './App.css';
import AppNavbar from './components/AppNavbar';
import Courses from './pages/Courses';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';

function App() {
  return (
    <div className="App">
      <>
      <AppNavbar/>

      <Container>
      {/* <Home/>
      <Courses/> 
      <Register></Register>*/}
      <Login></Login>
      </Container>
      </>
    </div>
  );
}

export default App;
